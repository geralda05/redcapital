import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '@/components/Inicio'
import Home from '@/components/Home'

Vue.use(Router);
 
var tokenSession = localStorage.getItem('acceso_token');

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path:"*",
      redirect:"/",
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      beforeEnter:(to, from, next) => {
        if (localStorage.acceso_token){
          next();
        }else{
          next('/'); 
        } 
      }, 
    }
  ]
})
